/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils_bonus.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 14:43:50 by qdam              #+#    #+#             */
/*   Updated: 2021/05/20 14:44:55 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

static void	*ft_memcpy(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*d;
	char	*s;

	i = 0;
	if (dst != (void *)src)
	{
		d = (char *)dst;
		s = (char *)src;
		while (i < len)
		{
			d[i] = s[i];
			i++;
		}
	}
	return (dst);
}

static void	*ft_realloc(void *src, size_t len_src, size_t len_dst)
{
	char	*dst;
	size_t	to_copy;

	dst = malloc(len_dst);
	if (dst)
	{
		if (len_src < len_dst)
			to_copy = len_src;
		else
			to_copy = len_dst;
		ft_memcpy(dst, src, to_copy);
	}
	free(src);
	return (dst);
}

static bool	read_to_buffer(t_data *data)
{
	size_t	n;
	ssize_t	br;

	n = 0;
	br = BFSZ;
	while (br == BFSZ)
	{
		if (!n)
			data->buf = malloc(BFSZ + 1);
		else
			data->buf = ft_realloc(data->buf, n * BFSZ + 1, (n + 1) * BFSZ + 1);
		if (!data->buf)
			return (false);
		br = read(data->fd, data->buf + n * BFSZ, BFSZ);
		if (br < 0)
			return (false);
		*(data->buf + n * BFSZ + br) = 0;
		n++;
	}
	return (true);
}

int	read_next(t_data *d, char **line)
{
	size_t	i;

	if (d->stt <= 0)
		return (d->stt);
	if (!d->i && !read_to_buffer(d))
		return (d->stt = ERROR);
	i = d->i;
	while (d->buf[i] && d->buf[i] != '\n')
		i++;
	*line = malloc(sizeof(char) * (i - d->i + 1));
	if (!(*line))
		return (d->stt = ERROR);
	ft_memcpy(*line, d->buf + d->i, i - d->i);
	(*line)[i - d->i] = 0;
	d->i = i + 1;
	if (d->buf[i] == 0)
		return (d->stt = ENDF);
	return (READ);
}
