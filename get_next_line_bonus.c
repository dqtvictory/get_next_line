/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 13:13:38 by qdam              #+#    #+#             */
/*   Updated: 2021/05/20 14:44:25 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

static bool	alloc_data(t_data **data_ptr, int fd)
{
	t_data	*data;

	data = malloc(sizeof(t_data));
	if (!data)
		return (false);
	data->fd = fd;
	data->stt = READ;
	data->buf = NULL;
	data->i = 0;
	data->next = NULL;
	*data_ptr = data;
	return (true);
}

static t_data	*search_create(t_data *begin, int fd)
{
	t_data	*last;

	while (begin)
	{
		if (begin->fd == fd)
			return (begin);
		last = begin;
		begin = begin->next;
	}
	if (alloc_data(&last->next, fd))
		return (last->next);
	return (NULL);
}

static void	search_free(t_data **begin)
{
	t_data	*cur;
	t_data	*tmp;

	cur = *begin;
	while (cur)
	{
		if (cur->stt == READ)
			return ;
		cur = cur->next;
	}
	cur = *begin;
	while (cur)
	{
		tmp = cur;
		cur = cur->next;
		free(tmp);
	}
	*begin = NULL;
}

int	get_next_line(int fd, char **line)
{
	static t_data	*data;
	t_data			*cur;
	int				rn;

	if (fd < 0 || !line || (!data && !alloc_data(&data, fd)))
		return (ERROR);
	cur = search_create(data, fd);
	if (!cur)
		return (ERROR);
	rn = read_next(cur, line);
	if (rn <= 0)
	{
		if (cur->buf)
			free(cur->buf);
		cur->buf = NULL;
		search_free(&data);
	}
	return (rn);
}
