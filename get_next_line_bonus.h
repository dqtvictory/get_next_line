/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/20 13:12:27 by qdam              #+#    #+#             */
/*   Updated: 2021/05/20 14:45:19 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 1024
# endif
# define BFSZ BUFFER_SIZE

# include <stdbool.h>
# include <stdlib.h>
# include <fcntl.h>
# include <unistd.h>

# define READ 1
# define ENDF 0
# define ERROR -1

typedef struct s_data
{
	int				fd;
	int				stt;
	char			*buf;
	size_t			i;
	struct s_data	*next;
}	t_data;

int		read_next(t_data *d, char **line);
int		get_next_line(int fd, char **line);

#endif
