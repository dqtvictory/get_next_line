/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/16 17:32:13 by qdam              #+#    #+#             */
/*   Updated: 2021/05/24 09:53:37 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

bool	alloc_data(t_data *data, int fd)
{
	data->fd = fd;
	data->stt = READ;
	data->buf = malloc(sizeof(char) * (BFSZ + 1));
	if (!data->buf)
		return (0);
	data->i = 0;
	return (1);
}

int	get_next_line(int fd, char **line)
{
	static t_data	data;
	int				rn;

	if (!data.buf && !alloc_data(&data, fd))
		return (ERROR);
	if (fd != data.fd || !line)
		return (ERROR);
	rn = read_next(&data, line);
	if (rn <= 0 && data.buf)
	{
		free(data.buf);
		data.buf = NULL;
	}
	return (rn);
}
